import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class FoodOrder {

    void order (String food, int price) {
        int  confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation==0) {
            String currentText = textPane1.getText();
            textPane1.setText(food + " " + price + " yen");
            String newText = textPane1.getText();
            textPane1.setText(currentText + newText + "\n");
            JOptionPane.showMessageDialog(null, "Thank you. Order for " + food +" received.");

            String currentPrice = Label1.getText();
            Label1.setText( price + " yen");
            String intPrice =currentPrice.replaceAll("[^0-9]", "");
            int newPrice=Integer.parseInt(intPrice)+ price;
            Label1.setText( newPrice + " yen");

        }
    }
    private JPanel root;
    private JButton ramen;
    private JButton tempura;
    private JButton potato;
    private JButton rice;
    private JButton frenchtoast;
    private JButton oolong;
    private JTextPane textPane1;
    private JButton Check;
    private JLabel Total;
    private JLabel Label1;

    public FoodOrder() {
    tempura.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Tempura",500);
        }
    });
        ramen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 760);
            }
        });
        potato.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Potato",350);
            }
        });
        rice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Rice",200);
            }
        });
        frenchtoast.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("French toast", 600);
            }
        });
        oolong.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Oolong tea",150);
            }
        });
        Check.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation==0) {
                    String finalPrice = Label1.getText();
                    JOptionPane.showMessageDialog(null, "Thank you! " +
                            "The total price is " + finalPrice + ".");
                    textPane1.setText("");
                    Label1.setText("0 yen");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrder");
        frame.setContentPane(new FoodOrder().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
